using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Microsoft.BotBuilderSamples
{
    /// <summary>
    /// Класс-диалог взаимодействия с пользователем по авторизации и получению информации.
    /// </summary>
    public class MainDialog : LogoutDialog
    {
        protected readonly ILogger Logger;
        private SimpleGraphClient _simpleGraphClient = null;
        private Timer _timer;
        private DateTimeOffset dateTimeStamp = DateTimeOffset.MinValue;

        public MainDialog(IConfiguration configuration, ILogger<MainDialog> logger)
            : base(nameof(MainDialog), configuration["ConnectionName"])
        {
            Logger = logger;

            AddDialog(new OAuthPrompt(
                nameof(OAuthPrompt),
                new OAuthPromptSettings
                {
                    ConnectionName = ConnectionName,
                    Text = "Please Sign In",
                    Title = "Sign In",
                    Timeout = 300000, // 5 минут пользователю на авторизацию
                }));

            AddDialog(new ConfirmPrompt(nameof(ConfirmPrompt)));

            AddDialog(new WaterfallDialog(nameof(WaterfallDialog), new WaterfallStep[]
            {
                PromptStepAsync,
                LoginStepAsync,
                GetMailStepAsync
                //DisplayTokenPhase1Async,
                //DisplayTokenPhase2Async,
            }));

            // Стартовый диалог.
            InitialDialogId = nameof(WaterfallDialog);
        }

        private async Task<DialogTurnResult> PromptStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.BeginDialogAsync(nameof(OAuthPrompt), null, cancellationToken);
        }

        private async Task<DialogTurnResult> LoginStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (_simpleGraphClient != null)
                return await stepContext.NextAsync(stepContext, cancellationToken);

            var tokenResponse = (TokenResponse)stepContext.Result;
            if (tokenResponse?.Token != null)
            {
                _simpleGraphClient = new SimpleGraphClient(tokenResponse.Token);

                var me = await _simpleGraphClient.GetMeAsync();
                var title = !string.IsNullOrEmpty(me.JobTitle) ?
                            me.JobTitle : "Unknown";

                await stepContext.Context.SendActivityAsync($"You're logged in as {me.DisplayName} ({me.UserPrincipalName}); you job title is: {title}");

                return await stepContext.NextAsync(stepContext, cancellationToken);
            }

            await stepContext.Context.SendActivityAsync(MessageFactory.Text("Login was not successful please try again."), cancellationToken);
            return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
        }

        private async Task<DialogTurnResult> GetMailStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            if (_simpleGraphClient != null && stepContext.Context.Activity.Type == ActivityTypes.Message)
            {
                var text = stepContext.Context.Activity.Text.ToLowerInvariant();
                if (text.IndexOf("mail") >= 0)
                {
                    var mails = await _simpleGraphClient.GetRecentMailAsync();
                    var unreadMails = mails.ToList().Where(x => x.IsRead.HasValue && x.IsRead.Value == false && x.ReceivedDateTime >= dateTimeStamp).ToList();
                    if (unreadMails.Count > 0)
                    {
                        foreach (var mail in unreadMails)
                        {
                            await stepContext.Context.SendActivityAsync($"Your recent mails: {mail.Subject}");
                        }
                    }
                    else
                        await stepContext.Context.SendActivityAsync(MessageFactory.Text("You have not new e-mails."), cancellationToken);

                    dateTimeStamp = DateTimeOffset.Now;
                }
            }

            return await stepContext.BeginDialogAsync(nameof(OAuthPrompt), cancellationToken: cancellationToken);
        }

        private async Task<DialogTurnResult> DisplayTokenPhase1Async(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            await stepContext.Context.SendActivityAsync(MessageFactory.Text("Thank you."), cancellationToken);

            var result = (bool)stepContext.Result;
            if (result)
            {
                // Call the prompt again because we need the token. The reasons for this are:
                // 1. If the user is already logged in we do not need to store the token locally in the bot and worry
                // about refreshing it. We can always just call the prompt again to get the token.
                // 2. We never know how long it will take a user to respond. By the time the
                // user responds the token may have expired. The user would then be prompted to login again.
                //
                // There is no reason to store the token locally in the bot because we can always just call
                // the OAuth prompt to get the token or get a new token if needed.
                return await stepContext.BeginDialogAsync(nameof(OAuthPrompt), cancellationToken: cancellationToken);
            }

            return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
        }

        private async Task<DialogTurnResult> DisplayTokenPhase2Async(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var tokenResponse = (TokenResponse)stepContext.Result;
            if (tokenResponse != null)
            {
                await stepContext.Context.SendActivityAsync(MessageFactory.Text($"Here is your token {tokenResponse.Token}"), cancellationToken);
            }

            return await stepContext.EndDialogAsync(cancellationToken: cancellationToken);
        }
    }
}
